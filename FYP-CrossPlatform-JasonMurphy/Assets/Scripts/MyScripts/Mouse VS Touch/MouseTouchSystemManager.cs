﻿using UnityEngine;
using System.Collections;

public class MouseTouchSystemManager : MonoBehaviour {

    private Vector3 aim;
    public bool useJoystick;
    public MouseTouchSystemInput systemInput;

    void Awake()
    {
        systemInput = new MouseTouchSystemInput();
        OperatingSystemDetection();
    }

    #region OS management
    private void OperatingSystemDetection()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            setupWindows();

        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            setupAndroid();
        }
    }

    void setupAndroid()
    {
        systemInput.OS = MouseTouchSystemInput.OperatingSystem.Android;
        if (GameObject.FindGameObjectWithTag("TouchUI"))
        {
            useJoystick = true;
        }
    }

    void setupWindows()
    {
        systemInput.OS = MouseTouchSystemInput.OperatingSystem.Windows;

        if (Input.GetJoystickNames().Length == 1)
        {
            useJoystick = true;
        }
        
    }

    #endregion

    #region input management

    int getTouchCount()
    {
        return Input.touchCount;
    }

    void setTouchInput(int touchCount)
    {
        if(touchCount == 1)
        {
            systemInput.TouchClick0 = Input.GetTouch(0).position;
            systemInput.isClickedOrTouched1 = true;
        }
        if (touchCount == 2)
        {
            systemInput.TouchClick1 = Input.GetTouch(1).position;
            systemInput.isClickedOrTouched2 = true;
        }
         
    }



    void setMouseClicks()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {   
            systemInput.isClickedOrTouched1 = true;
            systemInput.TouchClick0 = Input.mousePosition;
        }
        else if(!Input.GetKey(KeyCode.Mouse0))
        {
            systemInput.isClickedOrTouched1 = false;          
        }

        if (Input.GetKey(KeyCode.Mouse1))
        {
            systemInput.isClickedOrTouched2 = true;
            systemInput.TouchClick0 = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
        }
        else if (!Input.GetKey(KeyCode.Mouse1))
        {
            systemInput.isClickedOrTouched2 = false;
        }

    }

   
   
    void setWheelSpin()
    {
        systemInput.WheelSpin = Input.GetAxis("Mouse ScrollWheel");
    }


    public MouseTouchSystemInput getInputs()
    {
        //if(systemInput.OS == SystemInput.OperatingSystem.Android)
        //{
        //    setTouchInput(Input.touchCount);
        //}
         if (systemInput.OS == MouseTouchSystemInput.OperatingSystem.Windows)
        {        
            setMouseClicks();
            setWheelSpin();
        }

        return systemInput;
    }

    #endregion
}
