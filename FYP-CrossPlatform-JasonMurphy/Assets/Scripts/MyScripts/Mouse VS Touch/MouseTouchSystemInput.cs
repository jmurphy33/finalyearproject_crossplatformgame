﻿using UnityEngine;
using System.Collections;

public class MouseTouchSystemInput  {
    public enum OperatingSystem
    {
        Android,
        Windows
    }



	public Vector3 TouchClick0 { get; set; }   
    public Vector3 TouchClick1 { get; set; }
    public float WheelSpin { get; set; }
    public int TouchCount { get; set; }

    public bool isClickedOrTouched1 { get; set; }
    public bool isClickedOrTouched2 { get; set; }
    public bool isPressedOrTouched3 { get; set; }

    public OperatingSystem OS { get; set; } 
    public Touch[] Touches { get; set; }
}
