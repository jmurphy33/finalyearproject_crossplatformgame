﻿using UnityEngine;
using System.Collections;

public class ObjectSelecter : MonoBehaviour
{

    public GameObject selectedObject;
    float minScale = 0.4f;
    float maxScale = 3.0f;



    public void selectObject(Camera camera, Vector3 position)
    {

        Ray r = camera.ScreenPointToRay(position);
        RaycastHit hit;

        if (Physics.Raycast(r.origin, r.direction, out hit, 1000))
        {
            if (hit.collider.gameObject.tag == "Move")
            {

                changeColor(Color.red);
                selectedObject = hit.collider.gameObject;
                changeColor(Color.blue);
            }
        }

    }

    void changeColor(Color color)
    {
        if (selectedObject != null)
        {
            selectedObject.GetComponent<Renderer>().material.color = color;
        }

    }

    public void scaleObject(float sf)
    {
        if (sf > 0.4)
            selectedObject.transform.localScale = new Vector3(sf, sf, sf);
    }

    public void rotateObject(Vector3 pos)
    {
        selectedObject.transform.Rotate(new Vector3(pos.y,-pos.x,0) * Time.deltaTime * 20.0f);
    }


    public void moveObject(Vector3 pos)
    {    
        // get the horizontal plane
        Plane hPlane = new Plane(Camera.main.transform.forward, Vector3.zero);

        // get ray using first touch
        Ray ray = Camera.main.ScreenPointToRay(pos);
        float distance = 0.0f;
        if (hPlane.Raycast(ray, out distance))
        {
            // move object to where the ray returns a point using the distance
            selectedObject.transform.position = ray.GetPoint(distance);
        }
    }

}
