﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MVTPlayerController : MonoBehaviour
{

    private MouseTouchSystemInput si;
    private MouseTouchSystemManager sm;
    private ObjectSelecter objSel;

    public Camera camera;

    float dragSpeed = 0.1f;
    float horizonatalLimit = 100f;
    float verticalLimit = 100f;
    Vector3 startingPostion;
    Vector3 endPosition;
    float sf;
    Toggle toggle;

    void select()
    {
        if (si.isClickedOrTouched2)
        {
            objSel.selectObject(camera, si.TouchClick0);
        }
    }



    // Use this for initialization
    void Start()
    {
        sm = GetComponent<MouseTouchSystemManager>();
        objSel = GetComponent<ObjectSelecter>();
        toggle = GameObject.FindGameObjectWithTag("Movement").GetComponent<Toggle>();

    }

    // Update is called once per frame
    void Update()
    {
        si = sm.getInputs();

        select();
        
        #region scale
        if (Input.GetKey(KeyCode.S) && toggle.isOn)
        {
            setScaleFactor();
            objSel.scaleObject(sf);
        }
        #endregion

        if (si.isClickedOrTouched1 && !toggle.isOn)
        {
            objSel.rotateObject(si.TouchClick0);
        }


        if (si.isClickedOrTouched2 && !toggle.isOn)
        {
            objSel.moveObject(si.TouchClick0);
        }


    }

    void setScaleFactor()
    {
        if (sf < 0)
        {
            sf = 0;
        }
        else
        {
            sf += si.WheelSpin;
        }

    }
}
