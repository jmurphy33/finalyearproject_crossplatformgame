﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ProfilerLogger : MonoBehaviour {

	// Use this for initialization
	void Start () {

        // outputs a log file on the status of the frame rate
        string sceneName = SceneManager.GetActiveScene().name;
        string os = Application.platform.ToString();
        string name = os + " " + sceneName + ".log";
        Profiler.logFile = name;
        Profiler.enabled = true;
    }
}
