﻿// http://catlikecoding.com/unity/tutorials/frames-per-second/ used as reference

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FrameRate : MonoBehaviour
{
    // stores average frame rate per second
    public int AverageFPS { get; private set; }

    // stores the highest frame rate per second
    public int HighestFPS { get; private set; }

    // stores the lowest frame rate per second
    public int LowestFPS { get; private set; }
    // total frame rates
    public int range;
    // store collect of frame rates to iterate over
    int[] buffer;
    // keep track of place in buffer
    int index;

    /// <summary>
    /// Start a new buffer if current buffer is empty
    /// </summary>
    void InitializeBuffer()
    {
        if (range <= 0)
        {
            range = 1;
        }
        buffer = new int[range];
        index = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // if the buffer is null or if the buffers length does not equal the range
        if (buffer == null || buffer.Length != range)
        {
            // setup the buffer again
            InitializeBuffer();
        }
        // update the buffer values
        BufferUpdate();

        // determine the min,max and avg frame rates
        FrameRateCalculation();
    }

    /// <summary>
    /// Updates the frame rate buffer
    /// Uses unscaled time 
    /// </summary>
    void BufferUpdate()
    {
        // increase index each time called to allow the next
        // buffered frame equal time to complete the frame
        buffer[index++] = (int)(1f / Time.unscaledDeltaTime);

        // if the index has gone over the total range
        if (index >= range)
        {
            // reset the index
            index = 0;
        }
    }

    /// <summary>
    /// Calculate the avegae, highest and 
    /// lowest frame rates
    /// </summary>
    void FrameRateCalculation()
    {
        // total of combined frames
        int sum = 0;
        // will store the highest value
        int highest = 0;

        // will store the lowest value
        // max value is used to set initial value
        int lowest = int.MaxValue;

        // iterate over each frame in range
        for (int i = 0; i < range; i++)
        {
            // let frame equal the current interation into the buffer
            int frame = buffer[i];
           
            // total the frames so far
            sum += frame;

            // if current frame is larger than stored highest
            if (frame > highest)
            {
                // set highest
                highest = frame;
            }

            // if current frame is less than stored smallest 
            if (frame < lowest)
            {
                // set lowest to frame
                lowest = frame;
            }
        }

        // let the average equal the sum divided by the total range
        AverageFPS = sum / range;
        // set the global variable highest
        HighestFPS = highest;
        // set the global variable lowest
        LowestFPS = lowest;
    }

}
