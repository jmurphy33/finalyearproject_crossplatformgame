﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HardwareDisplay : MonoBehaviour
{

    public Hardware HW;
    public Text DeviceTypeText;

    // Use this for initialization
    void Start()
    {
        // getting all the hardware components to display

        string sys = "DEVICE-------------\n" +
                     "* Type: " + HW.DeviceType + "\n" +
                     "* Name: " + HW.DeviceName + "\n" +
                     "* System memory: "+ HW.SystemMemory + "\n"+
                     "GRAPHICS-------------\n" +
                     "* Name: " + HW.GraphicsDeviceName + "\n" +
                     "* Type: " + HW.GraphicsDeviceType + "\n" +
                     "* Memory: "+ HW.GraphicsMemorySize + "\n"+
                     "* Multi-Threaded: "+ HW.GraphicsThreaded + "\n"+
                     "PROCESSOR-------------\n" +
                     "* Type:" + HW.CPUType + "\n" +
                     "* Cores: " + HW.CPUCores + "\n" +
                     "* Frequency: " + HW.CPUFrequency + "\n"
                      
                     ;

        // set test 
        DeviceTypeText.text = sys;

    }


}
