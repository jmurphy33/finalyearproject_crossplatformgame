﻿using UnityEngine;
using System.Collections;

public class Hardware : MonoBehaviour {

    // variables on the systems hardware
    public string DeviceType { get; private set; }
    public string SystemMemory { get; private set; }
    public string DeviceName { get; private set; }
    public string GraphicsDeviceName { get; private set; }
    public string GraphicsDeviceType { get; private set; }
    public string GraphicsMemorySize { get; private set; }
    public string GraphicsThreaded { get; private set; }
    public string CPUType { get; private set; }
    public string CPUCores { get; private set; }
    public string CPUFrequency { get; private set; }
    

    // Use this for initialization
    void Awake () {
        DeviceType = SystemInfo.deviceType.ToString();
        SystemMemory = SystemInfo.systemMemorySize.ToString();
        DeviceName= SystemInfo.deviceName.ToString();
        GraphicsDeviceName = SystemInfo.graphicsDeviceName.ToString();
        GraphicsDeviceType = SystemInfo.graphicsDeviceType.ToString();
        CPUType = SystemInfo.processorType.ToString();
        CPUCores = SystemInfo.processorCount.ToString();
        CPUFrequency = SystemInfo.processorFrequency.ToString();
        GraphicsMemorySize = SystemInfo.graphicsMemorySize.ToString();
        GraphicsThreaded = SystemInfo.graphicsMultiThreaded.ToString();

    }
	
}
