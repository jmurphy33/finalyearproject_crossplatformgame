﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {


    public float timeBetweenSpawns;
    public float spawnDistance;
    public Ball[] ballArray;

    // time between objects being spawned
    float timeSinceLastSpawn;
    
    void FixedUpdate()
    {
        timeSinceLastSpawn += Time.deltaTime;
        if(timeSinceLastSpawn >= timeBetweenSpawns)
        {
            timeSinceLastSpawn -= timeBetweenSpawns;
            SpawnBall();
        }
    }

    /// <summary>
    ///  spawns new object to use in testing the frame rate
    /// </summary>
    void SpawnBall()
    {
        Ball b = ballArray[Random.Range(0, ballArray.Length)];
        Ball spawn = Instantiate<Ball>(b);
        spawn.transform.localPosition = Random.onUnitSphere * spawnDistance;
    }
}
