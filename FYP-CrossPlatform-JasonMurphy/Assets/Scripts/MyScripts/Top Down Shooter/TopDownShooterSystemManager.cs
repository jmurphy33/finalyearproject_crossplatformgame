﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class TopDownShooterSystemManager : MonoBehaviour {

    // entity class with standard variables for inputs
    public TopDownShooterSystemInput systemInput;
    // reference to canvas fo touch based controls
    public Canvas touchControls;
    // reference to android's button for reloading
    public Button androidReloadButton;
    // bool check if button is pressed for reload
    public bool reloadIsPressed;

    int buttonPressCount;
    int buttonOldCount;

    void Awake()
    {
        systemInput = new TopDownShooterSystemInput();
        OperatingSystemDetection();

    }


    public void OnReloadPress()
    {
        buttonPressCount++;
    }

    public void OnReloadNotPressed()
    {
        // if button released set to false
        reloadIsPressed = false;
    }

    private bool getReloadThroughGUI()
    {
        if(buttonOldCount < buttonPressCount)
        {
            buttonPressCount = 0;
            buttonOldCount = 0;
            return true;
        }
        else
        {
            return false;
        }
    }


    private void OperatingSystemDetection()
    {
        //if using windows in a player or editor
        if (Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.WindowsPlayer)
        {
            setupWindows();

        }
        // if using an android device
        else if (Application.platform == RuntimePlatform.Android)
        {
            setupAndroid();

            androidReloadButton = GameObject.Find("ReloadButton").GetComponent<Button>();
            androidReloadButton.onClick.AddListener(OnReloadPress);
        }
    }

    void setupAndroid()
    {
        // set operting system to android
        systemInput.OS = TopDownShooterSystemInput.OperatingSystem.Android;
        
    }

    void setupWindows()
    {
        // set operating system to windows
        systemInput.OS = TopDownShooterSystemInput.OperatingSystem.Windows;
       
    }  

    /// <summary>
    /// Multiplatform method to retrieve 
    /// the vector3 for moving axis
    /// </summary>
    /// <returns></returns>
    Vector3 getMovementInput()
    {
        Vector3 directionalMovement =
            new Vector3(CrossPlatformInputManager.
            GetAxisRaw("Horizontal"), 0, 
            CrossPlatformInputManager.GetAxisRaw("Vertical"));

        return directionalMovement;
    }

    /// <summary>
    /// Get windows aim input. This is 
    /// received from the mouse click
    /// </summary>
    /// <returns></returns>
    bool isMouseAiming()
    {
        return Input.GetButton("Fire1");
    }

    bool isReloading()
    {
        // for windows, reload is check by pressing r or not
        return Input.GetKey(KeyCode.R);
    }

    /// <summary>
    /// Returns the mouse position on the 
    /// screen
    /// </summary>
    /// <returns></returns>
    Vector3 getMousePosition()
    {
        return Input.mousePosition;
    }

    /// <summary>
    /// Returns the Vector3 for aimimg
    /// using the joystick axis'
    /// </summary>
    /// <returns></returns>
    Vector3 getAimJoyStickPosition()
    {
        float aimx, aimy;
        aimx = CrossPlatformInputManager.GetAxis("AimHorizontal");
        aimy = CrossPlatformInputManager.GetAxis("AimVertical");

        return new Vector3(aimx, aimy, 0);
    }

    /// <summary>
    /// checks to see if the joysticks
    /// for aiming have been moved
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    bool isJoystickAiming(Vector3 input)
    {
        if (input != Vector3.zero)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Sets the inputs for the device
    /// </summary>
    /// <returns></returns>

    public TopDownShooterSystemInput getInputs()
    {
       
        if (systemInput.OS == TopDownShooterSystemInput.OperatingSystem.Android)
        {
            
            // set inputs for reloading, moving, rotating, aiming 
            systemInput.HorizonalVerticalInput = getMovementInput();
            systemInput.AimPosition = getAimJoyStickPosition();
            systemInput.isAiming = isJoystickAiming(systemInput.AimPosition);
            systemInput.AimUsesCamera = false;
            systemInput.doReload = getReloadThroughGUI();
            
        }

        // set inputs for reloading, moving, rotating, mouse position, mouse clicked
        else if (systemInput.OS == TopDownShooterSystemInput.OperatingSystem.Windows)
        {
            systemInput.HorizonalVerticalInput = getMovementInput();
            systemInput.AimPosition = getMousePosition();
            systemInput.isAiming = isMouseAiming();
            systemInput.AimUsesCamera = true;
            systemInput.doReload = isReloading();
        }

        return systemInput;
    }

  
}
