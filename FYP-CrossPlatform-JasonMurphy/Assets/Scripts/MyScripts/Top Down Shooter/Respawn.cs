﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Respawn : NetworkBehaviour
{
    // reference to the player's health
    private PlayerHealth health;

    // reference to the respawn button in the GUI
    private GameObject respawnButton;

    /// <summary>
    /// Called before player starts to setup health references
    /// </summary>
    public override void PreStartClient()
    {
        // instantiates the health
        health = GetComponent<PlayerHealth>();
        // set the delegate for respawn event
        health.EventRespawn += EnablePlayer;
    }

    /// <summary>
    /// Called with the local player starts
    /// </summary>
    public override void OnStartLocalPlayer()
    {
        // Sets the respawn button when started
        SetRespawnButton();
    }

    /// <summary>
    /// Removes the delegate reference when the 
    /// Network is destroyed
    /// </summary>
    public override void OnNetworkDestroy()
    {
        // remove event delegate listener
        health.EventRespawn -= EnablePlayer;
    }

    /// <summary>
    /// Re-enables the players components when respawning
    /// </summary>
    void EnablePlayer()
    {
        // enable character controller to allow collision and movement
        GetComponent<CharacterController>().enabled = true;
        // turn on grpahics so player can be seen 
        transform.Find("Graphics").gameObject.SetActive(true);

        // if the player is local
        if (isLocalPlayer)
        {
            //enable the player controller for main player functionality
            GetComponent<TDSPlayerController>().enabled = true;
            // turn off respawn button
            respawnButton.SetActive(false);
        }

    }

    /// <summary>
    /// Instatiates the respawn button
    /// </summary>
    void SetRespawnButton()
    {
        if (isLocalPlayer)
        {
            // find ths respawn object through the game manager object, which stores references to objects
            respawnButton = GameObject.Find("GameManager").GetComponent<GameManager>().respawnButton;
            // add on click listener
            respawnButton.GetComponent<Button>().onClick.AddListener(startRespawn);
        }
    }

    void startRespawn()
    {
        // calls server to respawn player
        CmdRespawn();
    }

    [Command]
    void CmdRespawn()
    {
        // restore the health of the player to enable again 
        health.restoreHealth();
    }

}
