﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;


public class PlayerRotationSync : NetworkBehaviour {
    // sync the players rotation. hook method called on change
    [SyncVar(hook = "OnPlayerRotSynced")]
    private float syncPlayerRotation;

    // decides what type of lerping to do
    [SerializeField]
    private bool useHistoricalInterpolation;

    // sets the player transfor attached to this object. Set in editor
    [SerializeField]
    private Transform playerTransform;

    // rate to lerp rotation by
    private float lerpRate = 20;

    // to keep track of rotation
    private float lastPlayerRot;

    // to decide if should lerp
    private float threshold = 0.5f;

    // list of sycned rotations
    private List<float> syncPlayerRotList = new List<float>();

    // used to judge distance
    private float closeEnough = 0.4f;

    void Update()
    {
        // update remote player's rotation
        LerpRotation();
    }
	
	void FixedUpdate () {
        // send remote player's rotation
        TransmitRotations();
	}

    /// <summary>
    /// Lerps the player's rotation
    /// </summary>
    void LerpRotation()
    {
        if (!isLocalPlayer)
        {
            if (useHistoricalInterpolation)
            {
                HistoricalInterpolation();
            }
            else
            {
                OrdinaryLerping();
            }
        }
    }

    void HistoricalInterpolation()
    {
        // rotation list is not empty
        if(syncPlayerRotList.Count > 0)
        {
            // lerp rotation
            LerpRotation();
            // if the absolute value of current y rotation - sycned rotation 
            //is greater than close enough distance
            if(Mathf.Abs(playerTransform.localEulerAngles.y - syncPlayerRotList[0]) < closeEnough)
            {
                // remove first rotation in list
                syncPlayerRotList.RemoveAt(0);
            }
        }
    }

    /// <summary>
    /// Normal lerping - no list
    /// </summary>
    void OrdinaryLerping()
    {
        // normal lerp using synced rotation
        LerpPlayerRotation(syncPlayerRotation);
    }

    /// <summary>
    ///  normal lerp rotation given an angle as a float
    /// </summary>
    /// <param name="rotAngle"></param>
    void LerpPlayerRotation(float rotAngle)
    {
        Vector3 playerNewRotation = new Vector3(0, rotAngle, 0);
        playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, Quaternion.Euler(playerNewRotation), lerpRate * Time.deltaTime);
    }


    [Command]
    void CmdProvideRotationsToServer(float playerRot)
    {
        // let the sync rotation = current rotation of player
        syncPlayerRotation = playerRot;
    }

    [Client]
    void TransmitRotations()
    {
        if (isLocalPlayer)
        {
            // checks if greater than the threshhold using y value and last player rotation
            if (CheckIfBeyondThreshold(playerTransform.localEulerAngles.y, lastPlayerRot))
            {
                // last rotation is equal the player y rotation value
                lastPlayerRot = playerTransform.localEulerAngles.y;
                // call server to provide rotation
                CmdProvideRotationsToServer(lastPlayerRot);
            }
        }
    }

    /// <summary>
    /// Checks if beyond threshold
    /// </summary>
    /// <param name="rot1"></param>
    /// <param name="rot2"></param>
    /// <returns></returns>
    bool CheckIfBeyondThreshold(float rot1, float rot2)
    {
        // if the absolute value of rotation 1 - rotation 2 is 
        // greater that the threshold return true
        if(Mathf.Abs(rot1-rot2) > threshold)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /// <summary>
    /// Hool method to let the sync value equal
    /// last player rotation and add it to list
    /// </summary>
    /// <param name="latestPlayerRot"></param>
    [Client]
    void OnPlayerRotSynced(float latestPlayerRot)
    {
        syncPlayerRotation = latestPlayerRot;
        syncPlayerRotList.Add(syncPlayerRotation);
    }
}
