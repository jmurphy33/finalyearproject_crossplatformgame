﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Latency : NetworkBehaviour {
    // Reference to network client
    private NetworkClient client;
    // to store latency number
    private int lat;
    // Reference to text field to display latency
    private Text latText;

    /// <summary>
    /// When player starts initialize client and latency text field
    /// </summary>
    public override void OnStartLocalPlayer()
    {
        client = GameObject.Find("NetworkManager").GetComponent<NetworkManager>().client;
        latText = GameObject.Find("LatencyText").GetComponent<Text>();
    }


    void Update()
    {
        // updates text field
        ShowLatency();
    }

    /// <summary>
    /// Set text to latency value
    /// </summary>
    void ShowLatency()
    {
        lat = client.GetRTT();
        latText.text = "Latency: " + lat;
    }
}
