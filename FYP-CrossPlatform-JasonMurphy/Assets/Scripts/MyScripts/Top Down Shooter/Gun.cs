﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {

    public void Reload()
    {
        ammo = baseAmmo;
    }

    public enum Type
    {
        machinegun,
        pistol
    }


    public Type type;
    public string weaponName = "Weapon name";
    public int cost = 50;
    public string description;
    public float range = 100;
    public Color color = Color.yellow;
    public float fireRate = .5f;
    public int damage = 10;
    public int ammo = 30;
    private int baseAmmo = 30;
    public int rpm = 200;
    public GameObject spawn;
    public GameObject main;
    public ParticleSystem gunEffects;
    public LineRenderer bulletTrail;
    public AudioSource gunSoundFx;
    public Light gunLight;

}
