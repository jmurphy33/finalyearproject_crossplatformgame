﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerId : NetworkBehaviour {

    [SyncVar]
    public string name;
    
    private NetworkInstanceId netID;
    // keep track of player's transform
    private Transform playerTrans;


    public override void OnStartLocalPlayer()
    {
        GetID();
        SetID();
    }
    
    /// <summary>
    /// Sets the GameObject name
    /// to make it easier for the 
    /// network to find clients by name
    /// </summary>
    void SetID()
    {
        if (!isLocalPlayer)
        {
            playerTrans.name = name;
        }
        else
        {
            playerTrans.name = MakeName();
        }
    }

    void Update()
    {
        if(playerTrans.name == "" || playerTrans.name == "Soldier(Clone)")
        {
            SetID();
        }
    }

    [Client]
    void GetID()
    {
        // gets the id from network identity
        netID = GetComponent<NetworkIdentity>().netId;
        // calls server to make name
        CmdTellServerName(MakeName());
    }

    [Command]
    void CmdTellServerName(string n)
    {
        // sets name
        name = n;
    }

    string MakeName()
    {
        string n = "Player " + netID;
        return n;
    }

    // Use this for initialization
    void Awake () {
        playerTrans = transform;
	}
	
	
}
