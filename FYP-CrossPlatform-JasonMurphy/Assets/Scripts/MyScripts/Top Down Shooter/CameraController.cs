﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CameraController : MonoBehaviour
{
    public GameObject target;

    private Vector3 cameraTarget;
    Quaternion rotation;

    void Awake()
    {
        rotation = transform.rotation;
    }


    void Update()
    {
        cameraTarget = new 
            Vector3(target.transform.position.x, 
            transform.position.y, target.transform.position.z);
    }

    void LateUpdate()
    {
        transform.rotation = rotation;
        transform.position = Vector3.Lerp(transform.position, 
            cameraTarget, Time.deltaTime * 8);
    }


}