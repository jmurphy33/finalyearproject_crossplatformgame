﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerSetup : NetworkBehaviour
{

    public Behaviour[] remoteComponents;
    Camera camera;
    public LayerMask lm;
    string remoteLayerName = "RemotePlayer";

    // Use this for initialization
    void Start()
    {

        if (!isLocalPlayer)
        {
            // disable components so local Play cannot use them
            Disable();
            SetLayer();
        }
        else
        {
            camera = Camera.main;
            // turn off main cam
            if(camera != null)
            {

                camera.gameObject.SetActive(false);
            }
        }
        
    }
    

    void SetLayer()
    {
        // sets the remote players layer to RemotePlayer
        gameObject.layer = LayerMask.NameToLayer(remoteLayerName);

    }

    void Disable()
    {
        // disables all components in array
        for (int i = 0; i < remoteComponents.Length; i++)
        {
            remoteComponents[i].enabled = false;
        }
    }
}
