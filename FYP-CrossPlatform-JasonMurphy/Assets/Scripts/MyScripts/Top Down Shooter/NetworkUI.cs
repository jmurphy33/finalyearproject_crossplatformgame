﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NetworkUI : NetworkBehaviour {

    public GameObject Local;
    public NetworkManagerHUD hud;
    public Toggle toggle;
    
    void Update()
    {
        // swithes between Newtwork Manager HUD and custom HUD
        if (toggle.isOn)
        {
            Local.SetActive(false);
            hud.showGUI = true;
        }
        else
        {
            Local.SetActive(true);
            hud.showGUI = false;
        }
    }


}
