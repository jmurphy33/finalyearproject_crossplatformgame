﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Death : NetworkBehaviour
{
    // reference tothe players health component
    private PlayerHealth health;
    // reference to the players score component
    public Score s;

    /// <summary>
    /// Called before client starts to get health component
    /// and to set the die event delegate
    /// </summary>
    public override void PreStartClient()
    {
        health = GetComponent<PlayerHealth>();
        health.EventDie += DisablePlayer;
    }

    /// <summary>
    /// removes the delegate when the network is destroyed
    /// </summary>
    public override void OnNetworkDestroy()
    {
        health.EventDie -= DisablePlayer;
    }

  
    /// <summary>
    /// Turns off key componets when the player dies
    /// </summary>
    void DisablePlayer()
    {
        // disable character controler to stop collisions and movement
        GetComponent<CharacterController>().enabled = false;
        // hides graphics so player is invisible 
        transform.Find("Graphics").gameObject.SetActive(false);
        // set player to dead
        health.isDead = true;
        // set gameover 
        s.gamover = true;
        if (isLocalPlayer)
        {
            // disable player controler
            GetComponent<TDSPlayerController>().enabled = false;
            if(s.gamover)
            GameObject.Find("GameManager").GetComponent<GameManager>().respawnButton.SetActive(true);
        }

    }
}
