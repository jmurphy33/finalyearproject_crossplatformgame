﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Networking.Match;

public class NetworkController : NetworkManager {

    public Text ipInput;
    Text findGamesText;
    NetworkMatch match;

    void Start()
    {
    }

    void OnLevelWasLoaded(int level)
    {
        // if the level is the main menu
        if(level == 0)
        {
            // setup menu buttons
            StartCoroutine(SetupMenuSceneButtons());
        }
        else
        {
            // setup disconnect button
            SetupOtherSceneButtons();
        }
    }
    /// <summary>
    /// When the host starts create an new match
    /// </summary>
    public void StartupHost()
    {
        NetworkManager.singleton.StartHost();
        match = new NetworkMatch();
    }

    /// <summary>
    /// get the ip and port and start client
    /// </summary>
    public void JoinGame()
    {
        SetIPAddress();
        SetPort();
        NetworkManager.singleton.StartClient();
    }

    /// <summary>
    /// Set the ip address to the network address from input field
    /// </summary>
    void SetIPAddress()
    {
        GameObject InputFieldAddressText = GameObject.Find("InputFieldAddressText");
        if(InputFieldAddressText != null)
        {
            string ipAddress = InputFieldAddressText.GetComponent<Text>().text;
            NetworkManager.singleton.networkAddress = ipAddress;
        }
        
    }


    /// <summary>
    /// sets the port to 7777
    /// </summary>
    void SetPort()
    {
        NetworkManager.singleton.networkPort = 7777;
    }
    /// <summary>
    /// Sets or removes the on click listers tot the gui buttons
    /// for joining or hosting games
    /// </summary>
    /// <returns></returns>
    IEnumerator SetupMenuSceneButtons()
    {
        yield return new WaitForSeconds(0.3f);
        GameObject.Find("ButtonHostGame").GetComponent<Button>().onClick.RemoveAllListeners();
        GameObject.Find("ButtonHostGame").GetComponent<Button>().onClick.AddListener(StartupHost);
        GameObject.Find("ButtonJoinGame").GetComponent<Button>().onClick.RemoveAllListeners();
        GameObject.Find("ButtonJoinGame").GetComponent<Button>().onClick.AddListener(JoinGame);
    }

    /// <summary>
    /// Sets or removes the on click listeners for disconnect button
    /// </summary>
    void SetupOtherSceneButtons()
    {
        GameObject.Find("LeaveGameButton").GetComponent<Button>().onClick.RemoveAllListeners();
        GameObject.Find("LeaveGameButton").GetComponent<Button>().onClick.AddListener(NetworkManager.singleton.StopHost);
    }  

   
}
