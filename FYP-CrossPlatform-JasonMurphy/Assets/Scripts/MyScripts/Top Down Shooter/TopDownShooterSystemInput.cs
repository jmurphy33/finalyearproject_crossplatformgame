﻿using UnityEngine;
using System.Collections;

public class TopDownShooterSystemInput : MonoBehaviour {
    public enum OperatingSystem
    {
        Android,
        Windows
    }

    public OperatingSystem OS { get; set; }

    // used for basic movement and rotations
    // used for virtual joystic 1 or 
    // W,A,S,D or directional arrows
    public Vector3 HorizonalVerticalInput { get; set; }

    // used for rotations and aim based movement
    // used for virtual joystick 2 or mouse
	public Vector3 AimPosition { get; set; }  
    
    // used for shoot
    // if joystick 2 is not zero then true
    // if mouse0 is clicked then true 
    public bool isAiming { get; set; }

    // to determine which aim rotations to use
    public bool AimUsesCamera { get; set; }

    // to determine if should reload
    // Button clicks or R can set this to true
    public bool doReload { get; set; }
}
