﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;


public class TDSPlayerController : NetworkBehaviour
{
    public PlayerAnimator anim;
    private PlayerMotor playerMotor;
    private PlayerShoot playerShoot;
    private TopDownShooterSystemManager im;
    private TopDownShooterSystemInput si;

    // network syncs
    //private NetworkPlayerSync nms;
    private Vector3 HorizonalVerticalInput;
    private Vector3 rotation;

    private bool isContinousAiming;
    private bool isAiming;
    private Vector3 AimInput;

    void getInputs()
    {
        // get the standard inputs entity class
        si = im.getInputs();
    }

    void Start()
    {
        // initialize components
        anim = GetComponentInChildren<PlayerAnimator>();
        playerShoot = GetComponentInChildren<PlayerShoot>();
        playerMotor = GetComponent<PlayerMotor>();
        im = GetComponent<TopDownShooterSystemManager>();
    }

    void HandleShooing()
    {
        // if reload is pressed, then reload first
        if (si.doReload)
        {
            playerShoot.Reload();
        }
        // if is aiming, then shoot
        else if (si.isAiming)
        {
            playerShoot.Shoot();
        }
    }

    void HandleAnimation()
    {
        // if is aiming apply aim based animations
        if (si.isAiming)
        {
            anim.aimingMovement(HorizonalVerticalInput);
        }
        // if not aiming apply non aim based animations
        else
        {
            anim.normalMovement(HorizonalVerticalInput);
        }
    }

    void getVelocity()
    {
        // get the players 
        HorizonalVerticalInput = playerMotor.getVelocity(si.HorizonalVerticalInput);
    }
    
    void getRotation()
    {
        // if the player is aiming then apply aim based rotations using/not using camera
        if (si.isAiming)
        {
            rotation = playerMotor.getRotationWhileAiming(si.AimUsesCamera, si.AimPosition);
        }
        // if not player is not aiming use standard rotations.
        else if (si.HorizonalVerticalInput != Vector3.zero)
        {
            rotation = playerMotor.getRotation(si.HorizonalVerticalInput);
        }
    }

    void FixedUpdate()
    {
        getInputs();
        getVelocity();
        getRotation();
        HandleShooing();
        HandleAnimation();
        ApplyMovement(HorizonalVerticalInput);
        ApplyRotation(rotation);
    }

    /// <summary>
    /// Uses motor to move character
    /// </summary>
    /// <param name="vel"></param>
    private void ApplyMovement(Vector3 vel)
    {
        playerMotor.ApplyMovement(vel);
    }

    /// <summary>
    /// Uses motor to rotate character
    /// </summary>
    /// <param name="rot"></param>
    private void ApplyRotation(Vector3 rot)
    {
        playerMotor.ApplyRotation(rot);
    }

}


