﻿using UnityEngine;
using System.Collections;

public class PlayerMotor : MonoBehaviour {

    // movement variables
    private float speed = 7f;
    private CharacterController characterController;

    void Start()
    {
        // get the character controller for movement
        characterController = GetComponent<CharacterController>();
    }

    public Vector3 getVelocity(Vector3 input)
    {
        Vector3 motion = input;
        // apply speeds to inputs
        motion *= (Mathf.Abs(input.x) == 1 && Mathf.Abs(input.z) == 1) ? .7f : 1;
        motion *= speed;
        return motion;
    }

    public Vector3 getVelocityWhileAiming(Vector3 input)
    {
        return transform.InverseTransformDirection(getVelocity(input));
    }


   
    [SerializeField]
    private Camera cam;

    private float rotationSpeed = 650f;

    public Vector3 getRotation(Vector3 input)
    {
        // rotation used by both windows pc and android when not aiming

        // where to look
        Quaternion targetRotation = Quaternion.LookRotation(input);

        // get rotation
        Vector3 rot = Vector3.up *  Mathf.MoveTowardsAngle(transform.eulerAngles.y,  targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime);

        return rot;
    }

    public Vector3 getRotationWhileAiming(bool useCamera, Vector3 angle)
    {
        if (useCamera)
        {
            // if the player is on windows this will be used

            // get angle on camera to world point using mouse position
            Vector3 angleOnCam = cam.ScreenToWorldPoint(new Vector3(angle.x, angle.y,  cam.transform.position.y - transform.position.y));
            
            // where to look
            Quaternion targetRotation = Quaternion.LookRotation(angleOnCam -  new Vector3(transform.position.x, 0, transform.position.z));

            // get rotation
            Vector3 rotation = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, 
                targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime);

            // return rotation
            return rotation;
        }
        else if(!useCamera)
        {
            // if on android this will be used
            // where they are going
            float heading;
            
            heading = Mathf.Atan2(angle.x, angle.y);
            return new Vector3(0f, heading * Mathf.Rad2Deg, 0f);
        }

        else
        {
            return Vector3.zero;
        }
       
    }

    public void ApplyMovement(Vector3 _velocity)
    {
        // apply the found velocity to the players character controller
        characterController.Move(_velocity * Time.deltaTime);
    }

    public void ApplyRotation(Vector3 _rotation)
    {
        // apply the rotation found to the players transform
        if (_rotation != Vector3.zero)
            transform.eulerAngles = _rotation;
    }

}
