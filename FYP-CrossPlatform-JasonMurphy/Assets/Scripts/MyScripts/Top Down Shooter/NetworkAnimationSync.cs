﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkAnimationSync : NetworkBehaviour
{
    /// <summary>
    /// When player starts set NetworkAnimator parameters
    /// </summary>
    public override void OnStartLocalPlayer()
    {
        GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(1, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(2, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(3, true);
    }
    /// <summary>
    /// Before player starts set NetworkAnimator parameters
    /// </summary>
    public override void PreStartClient()
    {
        GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(1, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(2, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(3, true);
    }

}
