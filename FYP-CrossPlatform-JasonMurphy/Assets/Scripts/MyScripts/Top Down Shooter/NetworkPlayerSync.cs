﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkPlayerSync : NetworkBehaviour
{

    // movement variables
    [SyncVar]
    private Vector3 syncPosition;

    [SyncVar]
    private Quaternion syncRotation;

    [SerializeField]
    Transform playerTransform;

    private float lerpSpeed = 15f;

    public void LerpPlayer()
    {
        playerTransform.position = Vector3.Lerp(playerTransform.position, syncPosition, Time.deltaTime * lerpSpeed);
        playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, syncRotation, Time.deltaTime * lerpSpeed);
    }

    [Command]
    void CmdUpatePlayer(Vector3 myPosition, Quaternion myRotation)
    {
        syncPosition = myPosition;
        syncRotation = myRotation;
    }

    [ClientCallback]
    public void TransmitPlayer()
    {
        CmdUpatePlayer(playerTransform.position, playerTransform.rotation);
    }

    public override void OnStartLocalPlayer()
    {
        GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(1, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(2, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(3, true);
    }

    public override void PreStartClient()
    {
        GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(1, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(2, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(3, true);
    }

}
