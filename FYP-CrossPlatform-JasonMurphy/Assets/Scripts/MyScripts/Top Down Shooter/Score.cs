﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Score : NetworkBehaviour
{
    // to determine if the game is over 
    // sets to true when score is reached
    public bool gamover;
    // reference to text field to show players score
    private Text scoreText;
    // sync variable used to track players score
    // will use hook method when value changes
    [SyncVar(hook = "OnScoreChange")]
    public int playerScore;

    /// <summary>
    /// Initializes score text field and sets the text
    /// </summary>
    public override void OnStartLocalPlayer()
    {
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        SetScoreText();
    }

    /// <summary>
    /// Increase the players score by 1
    /// </summary>
    public void IncreaseScore()
    {
        playerScore++;

    }

    /// <summary>
    /// updates the score text field for the local player
    /// </summary>
    void SetScoreText()
    {
        if (isLocalPlayer)
        {
            scoreText.text = "Score: " + playerScore;
        }
    }

    /// <summary>
    /// hook method
    /// changes score to new score and sets text field 
    /// </summary>
    /// <param name="score"></param>
    void OnScoreChange(int score)
    {
        playerScore = score;
        SetScoreText();
    }


}
