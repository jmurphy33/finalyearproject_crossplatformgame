﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ScoreManager : NetworkBehaviour
{
    public int finalScore = 3;
    public Score score;


    void Start()
    {
        GameObject.Find("EndGameText").GetComponent<Text>().text = "";
    }

    void Update()
    {
        if (isLocalPlayer)
        {
            if (score.playerScore == finalScore)
            {
                CmdEndGame(transform.name);
            }
        }
    }

    [Command]
    public void CmdEndGame(string playername)
    {
        RpcDisable(playername);
    }

    [ClientRpc]
    void RpcDisable(string name)
    {
        score.gamover = true;
        GetComponent<CharacterController>().enabled = false;
        transform.Find("Graphics").gameObject.SetActive(false);
        GetComponent<TDSPlayerController>().enabled = false;
        GameObject.Find("EndGameText").GetComponent<Text>().text = "GAME OVER.\nWinner is: " + name + "! \nDisconnect to find a new Game";
        GameObject.Find("GameManager").GetComponent<GameManager>().respawnButton.SetActive(false);
    }
}
