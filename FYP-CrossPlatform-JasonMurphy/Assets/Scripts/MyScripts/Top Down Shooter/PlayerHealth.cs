﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerHealth : NetworkBehaviour
{
    // sync varialbe for health with hook method 
    [SyncVar(hook = "OnHealthChange")]
    private int health = 100;
    // reference to text field for health
    private Text healthText;
    // to check if the player was killed
    private bool shouldDie = false;
    // to check if the play is died
    public bool isDead = false;

    //  delegate for die event
    public delegate void DieDelegate();
    public event DieDelegate EventDie;
    // delegate for respawn event
    public delegate void RespawnDelegate();
    public event RespawnDelegate EventRespawn;
    // delelgate for scoring event
    public delegate void ScoreDelegate();
    public event ScoreDelegate EventScore;
    // to figure out who killed the player
    public string killedBy = "";
    // reference to partical system to play when hurt
    public ParticleSystem damageParticles;


    void Update()
    {
        // monitor if alive, dead, should be dead
        CheckCondition();
    }

    void CheckCondition()
    {

        // if the health i less that or
        // equal to zero and the player should be dying
        // but is not dead yet
        if (health <= 0 && !shouldDie && !isDead)
        {
            
            shouldDie = true;
        }

        // if health is gone and should be dead
        if (health <= 0 && shouldDie)
        {
            if (EventDie != null)
            {
                // call die event0
                EventDie();
                if(killedBy != "")
                {
                    GameObject.Find(killedBy).GetComponent<Score>().IncreaseScore();

                }
            }

            shouldDie = false;
        }
        // if health is positive and is dead
        if (health > 0 && isDead)
        {
            if (EventRespawn != null)
            {
                // respawn player
                EventRespawn();
            }

            isDead = false;
        }
    }
    /// <summary>
    ///  Initializes the health text fiel and set the text
    /// </summary>
    public override void OnStartLocalPlayer()
    {
        healthText = GameObject.Find("HealthText").GetComponent<Text>();
        SetHealthText();
    }

    /// <summary>
    /// if is local player than alter health text field
    /// </summary>
    void SetHealthText()
    {
        if (isLocalPlayer)
        {
            healthText.text = "Health: " + health;

        }
    }
    /// <summary>
    /// Uses a damage parameter to lower health
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="enemy"></param>
    /// <returns></returns>
    public int reduceHealth(int damage, string enemy)
    {
        
        health = health - damage;
        CmdOnDamage();

        if (health <= 0)
        {
            killedBy = enemy;
        }

        return health;
    }

    /// <summary>
    /// Alter health value if syncvar changes
    /// set text for health field
    /// </summary>
    /// <param name="h"></param>
    void OnHealthChange(int h)
    {
        health = h;
        SetHealthText();
    }

    [Command]
    void CmdOnDamage()
    {
        // to all players do partical effect
        RpcDoDamageEffect();
    }

    [ClientRpc]
    public void RpcDoDamageEffect()
    {
        // server starts corouting to display/hide 
        StartCoroutine("DamageEffect");
    }

    public void restoreHealth()
    {
        // bring health back up to 100 - used to respawn
        health = 100;     
    }

    IEnumerator DamageEffect()
    {

        damageParticles.Play();
        yield return new WaitForSeconds(.019f); ;
        damageParticles.Stop();
    }
}
