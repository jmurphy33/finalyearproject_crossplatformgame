﻿using UnityEngine;
using System.Collections;

public class PlayerAnimator : MonoBehaviour
{
    private Animator anim; 
    


    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }

    /// <summary>
    /// Sets aiming value for animator
    /// </summary>
    /// <param name="isAiming"></param>
    private void setAiming(bool isAiming)
    {
        if (anim != null)
        {
            anim.SetBool("Aiming", isAiming);
        }
           
    }
    /// <summary>
    /// Sets VSpeed value for animator
    /// </summary>
    /// <param name="vSpeed"></param>
    private void setVSpeed(float vSpeed)
    {
        if (anim != null)
        {
            anim.SetFloat("VSpeed", vSpeed);
        }
           
    }

    /// <summary>
    /// Sets HSpeed val for animator
    /// </summary>
    /// <param name="hSpeed"></param>
    private void setHSpeed(float hSpeed)
    {
        if (anim != null)
        {
            anim.SetFloat("HSpeed", hSpeed);
        }
            
    }

    /// <summary>
    ///  Set aiming movement for animator
    /// </summary>
    /// <param name="motion"></param>
    public void aimingMovement(Vector3 motion)
    {
        var local = transform.InverseTransformDirection(motion);
        setAiming(true);
        setHSpeed(local.x);
        setVSpeed(local.z);
    }

    /// <summary>
    /// Sets non aiming movement for animator
    /// </summary>
    /// <param name="motion"></param>
    public void normalMovement(Vector3 motion)
    {
        setAiming(false);
        setHSpeed(Mathf.Sqrt(motion.x * motion.x + motion.z * motion.z));
       
    }

   
}
