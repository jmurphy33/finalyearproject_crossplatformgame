﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerShoot : NetworkBehaviour
{
    // stores a reference to gun attributes
    public Gun weapon;

    // stores a reference to text field to display ammo count for current gun
    private Text ammoText;

    // used to detect when a player was hit with raycast
    RaycastHit shootHit;
    
    // stores a reference to the guns particle effects for muzzle flash
    ParticleSystem gunParticles;
    // stores a reference to the guns line renderer for bullet trail (not used)
    LineRenderer gunLine;
    // stores a reference to the audio clip on the gun to play shot
    AudioSource gunAudio;
    // mask is used to improve speed of determining what was hit by raycast. Set RemotePlayer
    public LayerMask mask;
    // time 
    float effectsDisplayTime = 0.2f;
    float timer;
    public float timeBetweenBullets = 0.15f;

    // Use this for initialization
    void Awake()
    {
        setComponets();
    }

    /// <summary>
    /// Initializes the componets from the gun object
    /// </summary>
    public void setComponets()
    {
        gunParticles = weapon.gunEffects;
        gunAudio = weapon.gunSoundFx;
        gunLine = weapon.bulletTrail;
        SetColors(weapon.color);
    }

    /// <summary>
    /// Determines if the player is allowed to
    /// shoot based on ammo count and timer cooldown
    /// </summary>
    /// <returns></returns>
    bool CanShoot()
    {
        // initial false will check if passes
        bool isAbleToShoot = false;

        // if timer is greater that fire rate timer and still have ammo
        if (timer > weapon.fireRate && weapon.ammo > 0)
        {
            // can shoot
            isAbleToShoot = true;
        }
        // return pass or fail
        return isAbleToShoot;
    }

    /// <summary>
    /// Shoot functionality for the player
    /// </summary>
    public void Shoot()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        timer += Time.deltaTime;

        if (CanShoot())
        {
            Fire();
        }
      
        
        //if (timer >= timeBetweenBullets * effectsDisplayTime)
        //{
        //    DisableEffects();
        //}
    }

    /// <summary>
    /// fires the player's gun
    /// reduces 1 bullet each time
    /// sets the timer to zero
    /// </summary>
    void Fire()
    {
        // reduce ammo
        weapon.ammo = weapon.ammo - 1;
       
        // reset timer
        timer = 0f;
       
        // get the forward from the current player
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
       
        // call the server to display effects for players gun
        CmdOnShoot();

        //  if hit remote player with raycast
        if (Physics.Raycast(transform.position, fwd, out shootHit, weapon.range, mask))
        {
            // get remote player's identy using gameobject name
            string unetIdentity = shootHit.transform.name;

            // call the server to take health from player that was shot
            CmdTellServerWhoWasShot(unetIdentity, weapon.damage, transform.name);

        }
    }

    /// <summary>
    /// Calls every client to display gun effects
    /// for player
    /// </summary>
    [Command]
    void CmdOnShoot()
    {
        RpcDoShootEffect();
    }

    /// <summary>
    ///  Starts effects for gun using coroutine
    /// </summary>
    [ClientRpc]
    void RpcDoShootEffect()
    {
       StartCoroutine("RenderTracer");
    }


    /// <summary>
    /// Get the player that was shot 
    /// to take health away
    /// </summary>
    /// <param name="enemyId"></param>
    /// <param name="damage"></param>
    /// <param name="playerId"></param>
    [Command]
    void CmdTellServerWhoWasShot(string enemyId, int damage, string playerId)
    {
        // find remote player using their gameobject name
        GameObject remotePlayer = GameObject.Find(enemyId);
        // get the remote player's health component to take health away
        remotePlayer.GetComponent<PlayerHealth>().reduceHealth(damage, playerId);
    }

    /// <summary>
    /// Turns on and off effects over a period
    /// of time
    /// </summary>
    /// <returns></returns>
    IEnumerator RenderTracer()
    {
        gunAudio.Play();
        gunLine.enabled = true;
        gunParticles.Play();
        yield return new WaitForSeconds(.019f); ;
        gunLine.enabled = false;
        gunParticles.Stop();
    }

    /// <summary>
    /// Reloads the gun
    /// </summary>
    public void Reload()
    {
        // calls the gun's reload method to refill ammo
        weapon.Reload();
    }

    /// <summary>
    /// Turns off effects for gun
    /// </summary>
    public void DisableEffects()
    {
        // Disable the line renderer and the light.
        gunLine.enabled = false;
    }

    /// <summary>
    /// Sets the colour of the gun effects
    /// </summary>
    /// <param name="color"></param>
    private void SetColors(Color color)
    {
        gunLine.material.color = color;
        gunParticles.startColor = color;
    }

    /// <summary>
    /// Updates the text field for ammo count
    /// </summary>
    private void SetAmmoText()
    {
        // not used
    }

    /// <summary>
    /// called before the client starts to find ammo text
    /// field 
    /// </summary>
    public override void PreStartClient()
    {
        ammoText = GameObject.Find("AmmoText").GetComponent<Text>();

    }

    /// <summary>
    /// Updates the ammo text field each frame
    /// </summary>
    void Update()
    {
        ammoText.text = "Ammo: " + weapon.ammo;
    }
}

