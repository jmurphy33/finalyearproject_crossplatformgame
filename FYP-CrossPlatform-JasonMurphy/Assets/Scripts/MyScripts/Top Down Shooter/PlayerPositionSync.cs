﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;

// sets to unreliable channel which is better for movement
[NetworkSettings (channel = 0, sendInterval = 0.1f)]
public class PlayerPositionSync : NetworkBehaviour {


    // sync position - hook method will be called when value changes
    [SyncVar(hook = "SyncPositionValues")]
    private Vector3 positionSync;

    // the tranform of the player. Set through editor
    [SerializeField]
    Transform myTransform;

    // to use ordinary or historical lerping
    [SerializeField]
    bool useHistoricalLerping = false;
   
    // how precise
    private float closeEnough = 0.11f;

    // rate to lerp players position 
    private float lerpRate = 15;
    // normal rate
    private float normalLerpRate = 16;
    // faster rate
    private float fastLerpRate = 27f;

    // to decide if should update
    private float threshold = 0.5f;

    // the players last postion
    private Vector3 lastKnownPosition;

    // current list of most up to date positions of player
    private List<Vector3> syncPositionList = new List<Vector3>();

	// Use this for initialization
	void Start () {
        lerpRate = normalLerpRate;
    }
	
	// Update is called once per frame
	void Update () {
        // to move the remote player for the local players view
        LerpPosition();
	}

 
    void FixedUpdate()
    {
        // to update the server of the players location
        TransmitPosition();
    }

    void LerpPosition()
    {
        // if remote player
        if (!isLocalPlayer)
        {
            if (useHistoricalLerping)
            {
                HistoricalLerping();
            }
            else
            {
                OrdinaryLerping();
            }
        }
    }

    [Command]
    void CmdProvidePositionToServer(Vector3 position)
    {
        // let the sycn position = current position
        positionSync = position;
    }

    [ClientCallback]
    void TransmitPosition()
    {
        // if this is the local plauer and the distance between the current position
        // and last known position are greater than the threshhold
        if(isLocalPlayer && Vector3.Distance(myTransform.position, lastKnownPosition) > threshold)
        {
            // call server to provide position 
            CmdProvidePositionToServer(myTransform.position);
            // update last know position of player to current position
            lastKnownPosition = myTransform.position;
        }
    }

    [Client]
    void SyncPositionValues(Vector3 newPosition)
    {
        // set the synced position to the new position 
        positionSync = newPosition;
        // add the synced position to the list
        syncPositionList.Add(positionSync);
    }

    /// <summary>
    /// Use ordinary lerping
    /// </summary>
    void OrdinaryLerping()
    {
        // update position using returned value from lerping current
        // position by the synced position using delta time by lerp rate
        myTransform.position = Vector3.Lerp(myTransform.position, positionSync,
            Time.deltaTime * lerpRate);

    }

    /// <summary>
    /// Using historical lerp
    /// </summary>
    void HistoricalLerping()
    {
        // if the list is not empty
        if(syncPositionList.Count > 0)
        {
            // set the position of the player to the return of lerping player's 
            //postion with the first synced position using delata time by lerp rate
            myTransform.position = Vector3.Lerp(myTransform.position, syncPositionList[0], Time.deltaTime * lerpRate);

            // if the distance between the current position and the position 
            // most recent in the sync list are not too close
            if(Vector3.Distance(myTransform.position, syncPositionList[0]) < closeEnough)
            {
                // remove first position
                syncPositionList.RemoveAt(0);
            }

            // if the list is greater that 10 apply the faster lerp to catch up
            if(syncPositionList.Count > 10)
            {
                lerpRate = fastLerpRate;
            }
            // otherwise always use the noral lerp speed
            else
            {
                lerpRate = normalLerpRate;
            }
        }
    }
}
